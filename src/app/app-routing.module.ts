import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './component/login/login.component';
import {RegisterComponent} from './component/register/register.component';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {
    path: 'hotels',
    loadChildren: './page/hotel/hotel.module#HotelModule'
  },
  {
    path: 'cosmetics',
    loadChildren: './page/cosmetics/cosmetics.module#CosmeticModule'
  },
  {
    path: 'medical',
    loadChildren: './page/medical/medical.module#MedicalModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
