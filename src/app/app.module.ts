import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {AuthService} from './service/auth.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {FooterComponent} from './component/footer/footer.component';
import {RegisterComponent} from './component/register/register.component';
import {HomeModule} from './page/home/home.module';
import {HeaderComponent} from './component/header/header.component';
import {JwtInterceptor} from './service/jwt-interceptor.service';
import {LoginComponent} from './component/login/login.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatNativeDateModule} from '@angular/material/core';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {
    path: '',
    loadChildren: './page/home/home.module#HomeModule'
  },
  {
    path: 'hotels',
    loadChildren: './page/hotel/hotel.module#HotelModule'
  },
  {
    path: 'cosmetics',
    loadChildren: './page/cosmetics/cosmetics.module#CosmeticsModule'
  },
  {
    path: 'medical',
    loadChildren: './page/medical/medical.module#MedicalModule'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    RegisterComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules}),
    FormsModule,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule,
    HomeModule,
    BrowserAnimationsModule,
    MatNativeDateModule
  ],
  providers: [
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
