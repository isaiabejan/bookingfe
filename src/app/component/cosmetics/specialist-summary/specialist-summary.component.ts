import {Component, Input} from '@angular/core';
import {SpecialistModel} from '../../../model/cosmetic/specialist.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-specialist-summary',
  templateUrl: './specialist-summary.component.html',
  styleUrls: ['./specialist-summary.component.scss']
})
export class SpecialistSummaryComponent {

  @Input()
  public specialist: SpecialistModel;

  constructor(private router: Router) {

  }

  viewProfile() {
    this.router.navigate(['specialist', this.specialist.id]);
  }
}
