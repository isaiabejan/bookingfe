import { Component, Input } from '@angular/core';
import { HotelModel } from '../../../model/hotel/hotel.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-hotel-summary',
  templateUrl: './hotel-summary.component.html',
  styleUrls: ['./hotel-summary.component.scss']
})
export class HotelSummaryComponent {

  @Input()
  public hotel: HotelModel;

  constructor(private router: Router) {

  }

  viewProfile() {
    this.router.navigate(['hotels', this.hotel.id]);
  }
}
