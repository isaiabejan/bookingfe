import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {AuthService} from '../../service/auth.service';
import {Router} from '@angular/router';
import {JwtRequestModel} from '../../model/user/jwt-request.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {

  form: FormGroup;
  user: JwtRequestModel = new JwtRequestModel();

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router) {

    this.form = this.fb.group({
      username: '',
      password: ''
    });
  }

  login() {
    this.user = this.form.value;
    if (this.user) {
      this.authService.login(this.user).subscribe((token: string) => {
        console.log('token: ' + token);
        localStorage.setItem('token', token);
        this.router.navigateByUrl('/');
      });
    }
  }

}
