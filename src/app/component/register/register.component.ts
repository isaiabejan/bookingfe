import {Component} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {UserModel} from '../../model/user/user.model';
import {AuthService} from '../../service/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  form: FormGroup;
  user: UserModel = new UserModel();

  constructor(private fb: FormBuilder,
              private authService: AuthService,
              private router: Router) {

    this.form = this.fb.group({
      username: '',
      password: ''
    });
  }

  register() {
    this.user = this.form.value;
    if (this.user) {
      this.authService.register(this.user).subscribe( () => {
        this.router.navigateByUrl('/login');
      });
    }
  }


}
