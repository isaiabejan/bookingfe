export class CosmeticServiceModel {
  id: number;
  name: string;
  price: number;
  description: string;
  duration: number;
}
