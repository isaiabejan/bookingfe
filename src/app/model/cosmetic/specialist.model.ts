export class SpecialistModel {
  id: number;
  name: string;
  location: string;
  type: string;
}
