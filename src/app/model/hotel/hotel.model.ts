export class HotelModel {
  id: number;
  name: string;
  location: string;
  lng: number;
  lat: number;
}
