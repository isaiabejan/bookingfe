export class ReservationModel {
  id: number;
  uderId: number;
  roomId: number;
  startDate: string;
  endDate: string;
}
