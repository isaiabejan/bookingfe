export class RoomModel {
  id: number;
  hotelId: number;
  persons: number;
  price: number;
  total: number;
}
