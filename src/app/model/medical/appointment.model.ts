export class AppointmentModel {
  id: number;
  userId: number;
  doctor: string;
  startDate: string;
  endDate: string;
}
