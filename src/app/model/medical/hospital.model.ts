export class HospitalModel {
  id: number;
  name: string;
  location: string;
  lat: number;
  lng: number;
}
