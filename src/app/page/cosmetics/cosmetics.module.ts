import {NgModule} from '@angular/core';
import {DashboardComponent} from './dashboard/dashboard.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {COSMETICS_ROUTES} from './cosmetics.routing';
import {CosmeticsService} from './cosmetics.service';
import {SpecialistListComponent} from './specialist/list/specialist-list.component';
import {SpecialistService} from './specialist/specialist.service';
import {SpecialistDetailsComponent} from './specialist/details/specialist-details.component';
import {SpecialistSummaryComponent} from '../../component/cosmetics/specialist-summary/specialist-summary.component';

@NgModule({
  declarations: [
    DashboardComponent,
    SpecialistListComponent,
    SpecialistDetailsComponent,
    SpecialistSummaryComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(COSMETICS_ROUTES)

  ],
  providers: [
    CosmeticsService,
    SpecialistService
  ],
  bootstrap: []
})
export class CosmeticsModule {

}
