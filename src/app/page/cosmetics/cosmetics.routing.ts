import { Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import {SpecialistListComponent} from './specialist/list/specialist-list.component';
import {SpecialistDetailsComponent} from './specialist/details/specialist-details.component';

export const COSMETICS_ROUTES: Routes = [
  { path: '', component: DashboardComponent },
  { path: ':type', component: SpecialistListComponent },
  { path: 'specialist/:id', component: SpecialistDetailsComponent }
];
