import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BASE_URL} from '../../../endpoint.const';

@Injectable()
export class CosmeticsService {

  constructor(private http: HttpClient) {
  }

  public getTypes(): Promise<string[]> {
    return this.http.get<string[]>(BASE_URL + 'cosmetics/types').toPromise();
  }
}
