import {Component, OnInit} from '@angular/core';
import {CosmeticsService} from '../cosmetics.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  types: string[] = ['hairstylist', 'barber', 'nails', 'makeup'];

  constructor(private cosmeticService: CosmeticsService,
              private router: Router) {

  }

  async ngOnInit() {
    // this.types = await this.cosmeticService.getTypes();
  }

  goTo(type: string) {
    this.router.navigate(['/cosmetics/', type]);
  }
}
