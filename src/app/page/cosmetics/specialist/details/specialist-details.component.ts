import {Component, OnInit} from '@angular/core';
import {SpecialistModel} from '../../../../model/cosmetic/specialist.model';
import {ActivatedRoute} from '@angular/router';
import {SpecialistService} from '../specialist.service';

@Component({
  selector: 'app-specialist-details',
  templateUrl: './specialist-details.component.html',
  styleUrls: ['./specialist-details.component.scss']
})
export class SpecialistDetailsComponent implements OnInit {

  public specialist: SpecialistModel;

  constructor(private route: ActivatedRoute,
              private specialistService: SpecialistService) {
  }

  async ngOnInit() {
    const id: string = this.route.snapshot.paramMap.get('id');
    this.specialist = await this.specialistService.getById(id);
  }

}
