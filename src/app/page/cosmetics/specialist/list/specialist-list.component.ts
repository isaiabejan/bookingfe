import {Component, OnInit} from '@angular/core';
import {SpecialistService} from '../specialist.service';
import {SpecialistModel} from '../../../../model/cosmetic/specialist.model';

@Component({
  selector: 'app-specialist-list',
  templateUrl: './specialist-list.component.html',
  styleUrls: ['./specialist-list.component.scss']
})
export class SpecialistListComponent implements OnInit {

  public specialists: SpecialistModel[];

  constructor(private specialistService: SpecialistService) {

  }

  async ngOnInit() {
    this.specialists = await this.specialistService.getAll();
  }

}
