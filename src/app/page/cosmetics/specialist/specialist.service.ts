import {Injectable} from '@angular/core';
import {HttpGenericService} from '../../../service/http-generic.service';
import {SpecialistModel} from '../../../model/cosmetic/specialist.model';
import {HttpClient} from '@angular/common/http';
import {BASE_URL, SPECIALIST_URL} from '../../../../endpoint.const';

@Injectable()
export class SpecialistService extends HttpGenericService<SpecialistModel> {

  constructor(http: HttpClient) {
    super(http, BASE_URL + SPECIALIST_URL);
  }

  public getAllByType(type: string): Promise<SpecialistModel[]> {
    return this.http.get<SpecialistModel[]>(BASE_URL + SPECIALIST_URL + type).toPromise();
  }

}
