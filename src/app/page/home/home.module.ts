import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HOME_ROUTES} from './home.routing';
import {HomeComponent} from './dashboard/home.component';
import {LoginComponent} from '../../component/login/login.component';

@NgModule({
  declarations: [
    HomeComponent,
    LoginComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(HOME_ROUTES)
  ],
  providers: [

  ],
  bootstrap: []
})
export class HomeModule {
}
