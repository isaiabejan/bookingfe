import {Routes} from '@angular/router';
import {HomeComponent} from './dashboard/home.component';
import {LoginComponent} from '../../component/login/login.component';

export const HOME_ROUTES: Routes = [
  { path: '', component: HomeComponent },
  { path: 'login', component: LoginComponent }
];
