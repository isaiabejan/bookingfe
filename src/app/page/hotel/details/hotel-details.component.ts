import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HotelService} from '../hotel.service';
import {HotelModel} from '../../../model/hotel/hotel.model';
import {RoomModel} from '../../../model/hotel/room.model';

@Component({
  selector: 'app-hotel-details',
  templateUrl: './hotel-details.component.html',
  styleUrls: ['./hotel-details.component.scss']
})
export class HotelDetailsComponent implements OnInit {

  public hotel: HotelModel;
  public rooms: RoomModel[];

  constructor(private route: ActivatedRoute,
              private hotelService: HotelService) {

  }

  async ngOnInit() {
    const id: string = this.route.snapshot.paramMap.get('id');
    this.hotel = await this.hotelService.getById(id);
    this.rooms = await this.hotelService.getRooms(id);
  }
}
