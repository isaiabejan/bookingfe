import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HotelListComponent} from './list/hotel-list.component';
import {RouterModule} from '@angular/router';
import {HOTEL_ROUTES} from './hotel.routing';
import {HotelSummaryComponent} from '../../component/hotel/hotel-summary/hotel-summary.component';
import {HotelService} from './hotel.service';
import {CommonModule} from '@angular/common';
import {HotelDetailsComponent} from './details/hotel-details.component';
import {RoomComponent} from './room/room.component';
import {RoomService} from './room/room.service';
import {ReservationService} from './reservation/reservation.service';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {DatepickerComponent} from '../../component/datepicker/datepicker.component';

@NgModule({
  declarations: [
    HotelListComponent,
    HotelSummaryComponent,
    HotelDetailsComponent,
    RoomComponent,
    DatepickerComponent
  ],
  imports: [
    FormsModule,
    NgbModule,
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(HOTEL_ROUTES),
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule
  ],
  providers: [
    HotelService,
    RoomService,
    ReservationService,
  ],
  bootstrap: []
})
export class HotelModule {
}
