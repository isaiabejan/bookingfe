import {Routes} from '@angular/router';
import {HotelListComponent} from './list/hotel-list.component';
import {HotelDetailsComponent} from './details/hotel-details.component';

export const HOTEL_ROUTES: Routes = [
  { path: '', component: HotelListComponent },
  { path: ':id', component: HotelDetailsComponent }
];
