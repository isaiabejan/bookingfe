import { Injectable } from '@angular/core';
import { HttpGenericService } from '../../service/http-generic.service';
import { HotelModel } from '../../model/hotel/hotel.model';
import { HttpClient } from '@angular/common/http';
import { BASE_URL, HOTEL_URL, ROOMS_URL } from '../../../endpoint.const';
import {RoomModel} from '../../model/hotel/room.model';

@Injectable()
export class HotelService extends HttpGenericService<HotelModel> {

  constructor(http: HttpClient) {
    super(http, BASE_URL + HOTEL_URL);
  }

  public getRooms(id: string): Promise<RoomModel[]> {
    return this.http.get<RoomModel[]>(BASE_URL + HOTEL_URL + id + '/' + ROOMS_URL).toPromise();
  }
}
