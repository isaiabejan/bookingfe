import {Component, OnInit} from '@angular/core';
import {HotelModel} from '../../../model/hotel/hotel.model';
import {HotelService} from '../hotel.service';

@Component({
  selector: 'app-hotel-list',
  templateUrl: './hotel-list.component.html',
  styleUrls: ['./hotel-list.component.scss']
})
export class HotelListComponent implements OnInit {

  public hotels: HotelModel[];

  constructor(private hotelService: HotelService) {

  }

  async ngOnInit() {
    this.hotels = await this.hotelService.getAll();
  }
}
