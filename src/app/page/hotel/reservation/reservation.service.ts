import {Injectable} from '@angular/core';
import {HttpGenericService} from '../../../service/http-generic.service';
import {ReservationModel} from '../../../model/hotel/reservation.model';
import {HttpClient} from '@angular/common/http';
import {BASE_URL, ROOM_RESERVATION_URL, SPECIALIST_URL} from '../../../../endpoint.const';

@Injectable()
export class ReservationService extends HttpGenericService<ReservationModel> {

  constructor(http: HttpClient) {
    super(http, BASE_URL + ROOM_RESERVATION_URL);
  }

}
