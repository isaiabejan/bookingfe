import {Component, OnInit} from '@angular/core';
import {RoomService} from './room.service';
import {ActivatedRoute} from '@angular/router';
import {RoomModel} from '../../../model/hotel/room.model';
import {ReservationModel} from '../../../model/hotel/reservation.model';

@Component({
  selector: 'app-room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss']
})
export class RoomComponent implements OnInit {
  public room: RoomModel;
  public reservations: ReservationModel[];

  constructor(private route: ActivatedRoute,
              private roomService: RoomService) {

  }

  async ngOnInit() {
    const id: string = this.route.snapshot.paramMap.get('id');
    this.room = await this.roomService.getById(id);
    this.reservations = await this.roomService.getReservations(id);
  }


}
