import { Injectable } from '@angular/core';
import { HttpGenericService } from '../../../service/http-generic.service';
import { HttpClient } from '@angular/common/http';
import { BASE_URL, ROOM_RESERVATION_URL } from '../../../../endpoint.const';
import {RoomModel} from '../../../model/hotel/room.model';
import {ReservationModel} from '../../../model/hotel/reservation.model';

@Injectable()
export class RoomService extends HttpGenericService<RoomModel> {
  constructor(http: HttpClient) {
    super(http, BASE_URL + ROOM_RESERVATION_URL);
  }

  public getReservations(id: string): Promise<ReservationModel[]> {
    return this.http.get<ReservationModel[]>(BASE_URL + ROOM_RESERVATION_URL + 'room' + id).toPromise();
  }
}
