import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {MedicalService} from '../medical.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  types: string[] = [];

  constructor(private medicalService: MedicalService,
              private router: Router) {

  }

  async ngOnInit() {
    // this.types = await this.cosmeticService.getTypes();
  }

  goTo(type: string) {
    this.router.navigate(['/medical/', type]);
  }
}
