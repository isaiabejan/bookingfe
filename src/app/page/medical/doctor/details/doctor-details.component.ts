import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DoctorService} from '../doctor.service';
import {DoctorModel} from '../../../../model/medical/doctor.model';

@Component({
  selector: 'app-specialist-details',
  templateUrl: './doctor-details.component.html',
  styleUrls: ['./doctor-details.component.scss']
})
export class DoctorDetailsComponent implements OnInit {

  public doctor: DoctorModel;

  constructor(private route: ActivatedRoute,
              private doctorService: DoctorService) {
  }

  async ngOnInit() {
    const id: string = this.route.snapshot.paramMap.get('id');
    this.doctor = await this.doctorService.getById(id);
  }

}
