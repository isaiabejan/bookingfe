import {Injectable} from '@angular/core';
import {HttpGenericService} from '../../../service/http-generic.service';
import {HttpClient} from '@angular/common/http';
import {BASE_URL, DOCTOR_URL, MEDICAL_URL} from '../../../../endpoint.const';
import {DoctorModel} from '../../../model/medical/doctor.model';

@Injectable()
export class DoctorService extends HttpGenericService<DoctorModel> {

  constructor(http: HttpClient) {
    super(http, BASE_URL);
  }

  public getAllByType(type: string): Promise<DoctorModel[]> {
    return this.http.get<DoctorModel[]>(BASE_URL + MEDICAL_URL + DOCTOR_URL + type).toPromise();
  }

}
