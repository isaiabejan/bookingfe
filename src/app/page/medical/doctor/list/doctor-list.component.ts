import {Component, OnInit} from '@angular/core';
import {DoctorModel} from '../../../../model/medical/doctor.model';
import {DoctorService} from '../doctor.service';

@Component({
  selector: 'app-specialist-list',
  templateUrl: './doctor-list.component.html',
  styleUrls: ['./doctor-list.component.scss']
})
export class DoctorListComponent implements OnInit {

  public doctors: DoctorModel[];

  constructor(private doctorService: DoctorService) {

  }

  async ngOnInit() {
    this.doctors = await this.doctorService.getAll();
  }

}
