import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HospitalService} from '../hospital.service';
import {HospitalModel} from '../../../../model/medical/hospital.model';

@Component({
  selector: 'app-specialist-details',
  templateUrl: './hospital-details.component.html',
  styleUrls: ['./hospital-details.component.scss']
})
export class HospitalDetailsComponent implements OnInit {

  public hospital: HospitalModel;

  constructor(private route: ActivatedRoute,
              private doctorService: HospitalService) {
  }

  async ngOnInit() {
    const id: string = this.route.snapshot.paramMap.get('id');
    this.hospital = await this.doctorService.getById(id);
  }

}
