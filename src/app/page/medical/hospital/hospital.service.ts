import {Injectable} from '@angular/core';
import {HttpGenericService} from '../../../service/http-generic.service';
import {HttpClient} from '@angular/common/http';
import {BASE_URL, HOSPITALS_URL, MEDICAL_URL} from '../../../../endpoint.const';
import {HospitalModel} from '../../../model/medical/hospital.model';

@Injectable()
export class HospitalService extends HttpGenericService<HospitalModel> {

  constructor(http: HttpClient) {
    super(http, BASE_URL);
  }

  public getAllByType(type: string): Promise<HospitalModel[]> {
    return this.http.get<HospitalModel[]>(BASE_URL + MEDICAL_URL + HOSPITALS_URL + type).toPromise();
  }

}
