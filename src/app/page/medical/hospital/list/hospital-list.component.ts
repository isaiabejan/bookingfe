import {Component, OnInit} from '@angular/core';
import {HospitalService} from '../hospital.service';
import {HospitalModel} from '../../../../model/medical/hospital.model';

@Component({
  selector: 'app-specialist-list',
  templateUrl: './hospital-list.component.html',
  styleUrls: ['./hospital-list.component.scss']
})
export class HospitalListComponent implements OnInit {

  public hospitals: HospitalModel[];

  constructor(private hospitalService: HospitalService) {

  }

  async ngOnInit() {
    this.hospitals = await this.hospitalService.getAll();
  }

}
