import {NgModule} from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MEDICAL_ROUTES} from './medical.routing';
import {MedicalService} from './medical.service';
import {DashboardComponent} from './dashboard/dashboard.component';
import {DoctorDetailsComponent} from './doctor/details/doctor-details.component';
import {DoctorListComponent} from './doctor/list/doctor-list.component';
import {HospitalDetailsComponent} from './hospital/details/hospital-details.component';
import {HospitalListComponent} from './hospital/list/hospital-list.component';
import {DoctorService} from './doctor/doctor.service';
import {HospitalService} from './hospital/hospital.service';

@NgModule({
  declarations: [
    DashboardComponent,
    DoctorDetailsComponent,
    DoctorListComponent,
    HospitalDetailsComponent,
    HospitalListComponent
  ],
  imports: [
    NgbModule,
    CommonModule,
    RouterModule.forChild(MEDICAL_ROUTES)
  ],
  providers: [
    MedicalService,
    DoctorService,
    HospitalService
  ],
  bootstrap: []
})
export class MedicalModule {

}
