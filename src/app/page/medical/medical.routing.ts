import {Routes} from '@angular/router';
import {DashboardComponent} from '../cosmetics/dashboard/dashboard.component';

export const MEDICAL_ROUTES: Routes = [
  { path: '', component: DashboardComponent }
];
