import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {JwtRequestModel} from '../model/user/jwt-request.model';
import {BASE_URL, LOGIN_URL, REGISTER_URL} from '../../endpoint.const';
import {UserModel} from '../model/user/user.model';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient) {

  }

  login(jwtRequest: JwtRequestModel) {
    return this.http.post(BASE_URL + LOGIN_URL, jwtRequest, { responseType: 'text' });
  }

  register(user: UserModel) {
    return this.http.post(BASE_URL + REGISTER_URL, user);
  }

}
