import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export abstract class HttpGenericService<ENTITY> {

  protected constructor(protected  http: HttpClient, protected apiUrl: string) {

  }

  public getAll(queryParams = null): Promise<ENTITY[]> {
    return this.http.get<ENTITY[]>(this.apiUrl).toPromise();
  }

  public getById(id: string, queryParams = null): Promise<ENTITY> {
    return this.http.get<ENTITY>(this.apiUrl + id).toPromise();
  }

  public save(body: any): Promise<ENTITY> {
    return this.http.post<ENTITY>(this.apiUrl, body).toPromise();
  }

  public update(body: any): Promise<ENTITY> {
    return this.http.put<ENTITY>(this.apiUrl, body).toPromise();
  }

  public delete(id: number): Promise<any> {
    return this.http.delete(this.apiUrl + id).toPromise();
  }

}
