import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BASE_URL, LOGIN_URL, REGISTER_URL} from '../../endpoint.const';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  escapedUrl: Array<string>;

  constructor() {
    this.escapedUrl = [BASE_URL + REGISTER_URL, BASE_URL + LOGIN_URL];
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!this.escapeUrl(req.url)) {
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${localStorage.getItem('token')}`
        }
      });
    }

    return next.handle(req);
  }

  private escapeUrl(url: string): boolean {
    return this.escapedUrl.includes(url);
  }
}
